
import pandas as pd
import datetime as dt
import numpy as np
import matplotlib.pyplot as plt
import ggplot
from ggplot import *
import squarify
import seaborn as sns

## Data Cleaning
df = pd.read_csv('yuan_dataset.csv',sep ="|")
df.head()
df['cont_id'].describe()
df['transaction_id'].describe()
df['transaction_date'].describe()
df['prod_price_net'].describe()
df['prod_id'].describe()

df.sort_values('cont_id')

df.dropna(inplace = True)
df.sort_values('cont_id')
df = df.drop(1834)

df.sort_values('transaction_id')
df.sort_values('transaction_date')
df.sort_values('prod_price_net')
df.sort_values('prod_id')

df['transaction_date']=pd.to_datetime(df['transaction_date'])
df['transaction_id'] = df['transaction_id'].astype(int)
df.head(10)


## Data Analysis
print(df['transaction_date'].min(), df['transaction_date'].max())

sd = dt.datetime(2019,11,1)
df['hist']=sd - df['transaction_date']
df['hist'].astype('timedelta64[D]')
df['hist']=df['hist'] / np.timedelta64(1, 'D')
df.head()
df=df[df['hist'] < 730]  # only select the recent 2 years
df.info()

rfmTable = df.groupby('cont_id').agg({'hist': lambda x:x.min(), # Recency
                                        'cont_id': lambda x: len(x),               # Frequency
                                        'prod_price_net': lambda x: x.sum()}) #Monateracy

rfmTable.rename(columns={'hist': 'Recency', 
                         'cont_id': 'Frequency', 
                         'prod_price_net': 'Monetary_value'
                        }, inplace=True)


rfmTable['Frequency'].sort_values()

quartiles = rfmTable.quantile(q=[0.25,0.50,0.75])
def RClass(x,p,d):
    if x <= d[p][0.25]:
        return 4
    elif x <= d[p][0.50]:
        return 3
    elif x <= d[p][0.75]: 
        return 2
    else:
        return 1


def FMClass(x,p,d):
    if x <= d[p][0.25]:
        return 1
    elif x <= d[p][0.50]:
        return 2
    elif x <= d[p][0.75]: 
        return 3
    else:
        return 4   

rfmSeg = rfmTable
rfmSeg['R_Quartile'] = rfmSeg['Recency'].apply(RClass, args=('Recency',quartiles,))
rfmSeg['F_Quartile'] = rfmSeg['Frequency'].apply(FMClass, args=('Frequency',quartiles,))
rfmSeg['M_Quartile'] = rfmSeg['Monetary_value'].apply(FMClass, args=('Monetary_value',quartiles,))
#rfmSeg['F_Quartile'].unique()




###########Visualization
#### Boxplot
fig, ax = plt.subplots(1, 3, figsize=(18, 5))
rfmSeg.boxplot('Recency', ax=ax[0], showfliers= True)
rfmSeg.boxplot('Frequency', ax=ax[1], showfliers= True)
rfmSeg.boxplot('Monetary_value', ax=ax[2], showfliers= True)
plt.show()

### Alternative of boxplot
#plt.figure(figsize=(10,10))
## Plot distribution of R
#plt.subplot(3, 1, 1); sns.distplot(rfmSeg['Recency'])
## Plot distribution of F
#plt.subplot(3, 1, 2); sns.distplot(rfmSeg['Frequency'])
## Plot distribution of M
#plt.subplot(3, 1, 3); sns.distplot(rfmSeg['Monetary_value'])
## Show the plot
#plt.show()

quartiles=quartiles.to_dict()
print(quartiles)


rfmSeg['RFMSeg'] = rfmSeg.R_Quartile.map(str)+ rfmSeg.F_Quartile.map(str) + rfmSeg.M_Quartile.map(str)
rfmSeg.sort_values(by=['RFMSeg', 'Monetary_value'], ascending=[False, False])
rfmSeg['Total Score'] = rfmSeg['R_Quartile'] + rfmSeg['F_Quartile'] +rfmSeg['M_Quartile']
#print(rfmSeg.head(), rfmSeg.info())
#RF_slice = rfmSeg.loc[:,'Monetary_value':'M_Quartile']
#RF_slice.groupby(['R_Quartile','F_Quartile']).agg({'M_Quartile':'mean'}).unstack()



# Define rfm_level function
def rfm_level(df):
    if df['Total Score'] >= 9:
        return '1.Can\'t Loose Them'
    elif ((df['Total Score'] >= 8) and (df['Total Score'] < 9)):
        return '2.Champions'
    elif ((df['Total Score'] >= 7) and (df['Total Score'] < 8)):
        return '3.Loyal'
    elif ((df['Total Score'] >= 6) and (df['Total Score'] < 7)):
        return '4.Potential'
    elif ((df['Total Score'] >= 5) and (df['Total Score'] < 6)):
        return '5.Promising'
    else:
        return '6.Require Activation'


rfmSeg['RFM_Level'] = rfmSeg.apply(rfm_level, axis=1)


# Calculate average values for each RFM_Level, and return a size of each segment 
rfm_level_agg = rfmSeg.groupby('RFM_Level').agg({
    'Recency': 'mean',
    'Frequency': 'mean',
    'Monetary_value': ['mean', 'count']
}).round(1)
# Print the aggregated dataset

#rfm_level_agg.columns = rfm_level_agg.columns.droplevel()
rfm_level_agg.columns = ['R_Mean','F_Mean','M_Mean', 'Count']
from tabulate import tabulate
print(tabulate(rfm_level_agg, headers='keys', tablefmt='markdown'))




### Sizeplot on customer segment
#Create our plot and resize it.
fig = plt.gcf()
ax = fig.add_subplot()
fig.set_size_inches(10, 6)
squarify.plot(sizes=rfm_level_agg['Count'], 
              label=['1.Can\'t Loose Them \n 489',
                     '2.Champions\n 214',
                     '3.Loyal\n 255',
                     '4.Potential\n 368',
                     '5.Promising\n 368', 
                     '6.Require Activation \n 388'], alpha=0.7, text_kwargs={'fontsize':14})
plt.title("RFM Segments on {} Customers".format(int(sum(rfm_level_agg['Count']))),fontsize=18,fontweight="bold")
plt.axis('off')
plt.show()


### Monatery score for each combination of Frequency and Recency

gg=ggplot(rfmSeg,aes('M_Quartile')) + geom_bar()+ facet_grid('R_Quartile','F_Quartile')+xlab('Monetary') + ylab('Recency') +ggtitle('Frequency')
gg.show()

#### heatmap for customer value in each category

rfmSeg['RF'] = rfmSeg.R_Quartile.map(str)+ rfmSeg.F_Quartile.map(str)


temp = pd.DataFrame(rfmSeg.groupby(['RF','R_Quartile','F_Quartile']).agg('Monetary_value').mean())
temp = temp.groupby(['R_Quartile','F_Quartile']).agg('sum').unstack()
temp.columns = ['F=1','F=3','F=4']
temp.index = ['R=1','R=2','R=3','R=4']

fig, ax = plt.subplots(figsize=(9,6)) 
ax =sns.heatmap(temp, annot=True, linewidths=0,vmin=0, vmax=250000,ax=ax,fmt='g')
label_y = ax.get_yticklabels()
plt.setp(label_y, rotation=0)
label_x = ax.get_xticklabels()
plt.setp(label_x, rotation=0)
plt.show()
###Heatmap in bar chart
#rfmSeg.groupby(['RFMSeg']).agg('Monetary_value').mean().plot(kind='bar', colormap='Blues_r',figsize=(10,6),fontsize =12)


## R,F,M distribution by Total Scores (optional)

rfmSeg.groupby(['Total Score','RFM_Level']).agg('Monetary_value').mean().plot(kind='bar', colormap='Blues_r',rot=90)
rfmSeg.groupby(['Total Score','RFM_Level']).agg('Frequency').mean().plot(kind='bar', colormap='Blues_r')
rfmSeg.groupby(['Total Score','RFM_Level']).agg('Recency').mean().plot(kind='bar', colormap='Blues_r')

