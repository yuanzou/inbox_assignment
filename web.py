#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 20 23:41:09 2020

@author: holasoylocaaa
"""

import os
from flask import Flask, render_template, request
from werkzeug.utils import secure_filename

app = Flask(__name__)

app.config['UPLOAD_FOLDER'] = 'source'


@app.route('/')
def upload_file1():
    return render_template('upload.html')


@app.route('/', methods=['GET', 'POST'])
def upload_file2():
    if request.method == 'POST':
        f = request.files['file']
        f.save(os.path.join(app.config['UPLOAD_FOLDER'], secure_filename(f.filename)))

    return ("file saved!")



if __name__ == '__main__':
    app.run(debug=True)
